//Student Number: 2137374
package application;
import vehicles.Bicycle;
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] Bicycle = new Bicycle[4];
        Bicycle[0] = new Bicycle("Specialized",18,30);
        Bicycle[1] = new Bicycle("Specialized",21,40);
        Bicycle[2] = new Bicycle("Unspecialized",10,15);
        Bicycle[3] = new Bicycle("Unspecialized",25,50);

        for (int i=0; i<Bicycle.length; i++){
            System.out.println(Bicycle[i].toString());
        }
    }
}
