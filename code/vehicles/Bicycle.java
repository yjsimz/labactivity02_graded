//Student Number: 2137374
package vehicles;

public class Bicycle {
    String manufacturer;
    int numberGears;
    double maxSpeed;

    public Bicycle(String UserManufacturer, int UserNumberGears, double UserMaxSpeed) {
        this.manufacturer = UserManufacturer;
        this.numberGears = UserNumberGears;
        this.maxSpeed = UserMaxSpeed;
    }

    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumberGears(){
        return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public String toString(){
        return "Manufacturer:" + this.manufacturer + ", " + "Number of Gears:" + this.numberGears + ", " + "MaxSpeed:" + this.maxSpeed; 
    }
}